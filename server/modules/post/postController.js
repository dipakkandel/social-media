const postSchema = require('./postSchema');
const objectId = require('mongoose').Types.ObjectId;
const otherHelper = require('../../helper/other.helper');
const httpStatus = require('http-status');
const commentSchema = require('../comment/commentSchema');
let postController = {};

postController.addNewPost = async (req, res, next) => {
  try {
    let data = req.body;
    if (!data.title && data.description) {
      return otherHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, 'Title or Description missing');
    }
    data.added_at = new Date();
    const newPost = new postSchema(data);
    const postSave = await newPost.save();
    return otherHelper.sendResponse(res, httpStatus.OK, true, { _id: postSave._id, title: postSave.title, description: postSave.description, added_at: postSave.added_at }, null, 'Post Saved Successfully');
  } catch (err) {
    next(err);
  }
};

postController.deletePost = async (req, res, next) => {
  try {
    const deletedPost = await postSchema.findOneAndUpdate(
      { _id: req.params.id, added_by: req.user.id },
      {
        $set: { is_deleted: true },
      },
      { new: true },
    );
    console.log(deletedPost);
    if (deletedPost && deletedPost._id) {
      return otherHelper.sendResponse(res, httpStatus.OK, true, deletedPost, null, 'Post Deleted Successfully');
    }

    return otherHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, 'Task Failed');
  } catch (err) {
    next(err);
  }
};

postController.likePost = async (req, res, next) => {
  try {
    let user = req.user.id;
    let post_id = req.params.id;
    const likedPost = await postSchema.findOneAndUpdate(
      { _id: post_id },
      {
        $push: { liked_by: objectId(user) },
        $inc: { likes: 1 },
      },
    );
    return otherHelper.sendResponse(res, httpStatus.OK, true, likedPost, null, 'Liked Successfully');
  } catch (err) {
    next(err);
  }
};
postController.unLikePost = async (req, res, next) => {
  try {
    let user = req.user.id;
    let post_id = req.params.id;
    const unLikedPost = await postSchema.findOneAndUpdate(
      { _id: post_id },
      {
        $pull: { liked_by: objectId(user) },
        $inc: { likes: -1 },
      },
      {
        new: true,
      },
    ).lean;
    return otherHelper.sendResponse(res, httpStatus.OK, false, unLikedPost, null, 'Unliked Successfully');
  } catch (err) {
    next(err);
  }
};

postController.getlikescomments = async (req, res, next) => {
  try {
    let postId = req.params.id;
    let postDetails = await postSchema
      .find({ _id: postId })
      .select('title description added_by added_at likes ')
      .populate([{ path: 'comment', select: 'comment added_by added_at' }]);
    return otherHelper.sendResponse(res, httpStatus.OK, true, postDetails, null, 'Post Details get success');
  } catch (err) {
    next(err);
  }
};
module.exports = postController;
