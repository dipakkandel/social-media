const mongoose = require('mongoose');
const schema = mongoose.Schema;

const postSchema = new schema({
  title: { type: String },
  description: { type: String },
  added_by: { type: schema.Types.ObjectId, ref: 'user' },
  added_at: { type: Date, default: Date.now },
  is_deleted: { type: Boolean, default: false },
  liked_by: [{ type: schema.Types.ObjectId, ref: 'user' }],
  likes: { type: Number, default: 1 },
  comment: [{ type: schema.Types.ObjectId, ref: 'comment' }],
});

module.exports = post = mongoose.model('post', postSchema);
