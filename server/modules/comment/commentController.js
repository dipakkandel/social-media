const commentSchema = require('./commentSchema');

const otherHelper = require('../../helper/other.helper');
const httpStatus = require('http-status');
const postSchema = require('../post/postSchema');
let commentController = {};

commentController.addComment = async (req, res, next) => {
  try {
    let post_id = req.params.id;
    let user = req.user.id;
    let comment = req.body.comment ? req.body.comment : null;
    if (comment == null) {
      return otherHelper.sendResponse(res, httpStatus.OK, true, null, 'Comment is missing');
    }
    let commentData = {
      post: post_id,
      comment: comment,
      added_by: user,
      added_at: new Date(),
    };
    const newComment = commentSchema(commentData);
    const saveComment = await newComment.save();
    const postUpdate = await postSchema.findByIdAndUpdate(post_id, {
      $push: { comment: saveComment._id },
    });
    console.log(saveComment);
    return otherHelper.sendResponse(res, httpStatus.OK, true, { _id: saveComment._id }, null, 'Comment Added Successfully');
  } catch (err) {
    next(err);
  }
};

module.exports = commentController;
