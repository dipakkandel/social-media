const mongoose = require('mongoose');
const schema = mongoose.Schema;

const commentSchema = new schema({
  post: { type: schema.Types.ObjectId, ref: 'post' },
  comment: { type: String, required: true },
  added_by: { type: schema.Types.ObjectId, ref: 'user' },
  added_at: { type: Date, default: Date.now },
});
module.exports = comment = mongoose.model('comment', commentSchema);
