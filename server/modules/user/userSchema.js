const mongoose = require('mongoose');
const schema = mongoose.Schema;

const userSchema = new schema({
  name: { type: String },
  email: { type: String },
  password: { type: String },
  gender: { type: String },
  post: [{ type: schema.Types.ObjectId, ref: 'posts' }],
  follower: [{ type: schema.Types.ObjectId, ref: 'user' }],
  following: [{ type: schema.Types.ObjectId, ref: 'user' }],
  is_deleted: { type: Boolean, default: false },
  deleted_at: { type: Date },
  mobile_no: { type: String },
});

module.exports = user = mongoose.model('user', userSchema);
