const userSchema = require('./userSchema');
const jwt = require('jsonwebtoken');
const objectId = require('mongoose').Types.ObjectId;

const otherHelper = require('../../helper/other.helper');
const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');
const postSchema = require('../post/postSchema');
const commentSchema = require('../comment/commentSchema');
let userController = {};

userController.returnToken = async (req, res, next) => {
  try {
    let data = req.body;
    let payload = {
      user: data.email,
      password: data.password,
    };
    let token = await jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1d' });
    return otherHelper.sendResponse(res, httpStatus.OK, true, token, null, 'success');
  } catch (err) {
    next(err);
  }
};

userController.processToken = async (req, res, next) => {
  try {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization || req.headers.token;
    const data = await jwt.verify(token, process.env.JWT_SECRET);
    otherHelper.sendResponse(res, httpStatus.OK, true, data, null, 'success');
  } catch (err) {
    next(err);
  }
};
userController.allUsers = async (req, res, next) => {
  let data = await userSchema.find({});
  return otherHelper.sendResponse(res, httpStatus.OK, true, data, null, 'users get successful');
};

userController.newUser = async (req, res, next) => {
  try {
    let data = req.body;
    if (data._id) {
      const update = await userSchema.findByIdAndDelete(
        { _id: data._id },
        {
          $set: data,
        },
      );
      return otherHelper.sendResponse(res, httpStatus.OK, true, update, null, 'User updated');
    }
    let email = req.body.email ? req.body.email : null;
    let password = req.body.password ? req.body.password : null;
    let mobile_no = req.body.mobile_no ? req.body.mobile_no : null;
    if (email == null || password == null || mobile_no == null) {
      return otherHelper.sendResponse(res, httpStatus.BAD_REQUEST, null, null, 'Email or password can not be empty');
    }
    const checkEmail = await userSchema.findOne({ email: data.email });
    console.log(checkEmail);
    if (checkEmail && checkEmail.length) {
      return otherHelper.sendResponse(res, httpStatus.CONFLICT, false, null, 'User email already exists');
    }
    bcrypt.hash(password, 10, async function (err, hash) {
      if (err) {
        return next(err);
      }
      data.password = hash;
      data.added_at = new Date();
      console.log(data);
      const newUser = new userSchema(data);
      const userSave = await newUser.save();
      return otherHelper.sendResponse(res, httpStatus.OK, true, userSave, null, 'User Saved Successfully');
    });
  } catch (err) {
    next(err);
  }
};

userController.login = async (req, res, next) => {
  try {
    let checkUser = await userSchema.findOne({ email: req.body.email });
    if (!checkUser) {
      return otherHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, 'Invalid Credentials email');
    }

    const passwordValid = await bcrypt.compare(req.body.password, checkUser.password);
    console.log(passwordValid);
    if (!passwordValid) {
      return otherHelper.sendResponse(res, httpStatus.BAD_REQUEST, false, null, 'Invalid Credentials');
    }
    const token = jwt.sign({ id: checkUser._id }, process.env.JWT_SECRET, { expiresIn: '1d' });
    return otherHelper.sendResponse(res, httpStatus.OK, true, token, null, 'Login Successful');
  } catch (err) {
    next(err);
  }
};

userController.follow = async (req, res, next) => {
  try {
    let id = req.params.id;
    console.log(id);
    const followerUpdate = await userSchema.findByIdAndUpdate(
      id,
      {
        $push: {
          follower: req.user.id,
        },
      },
      { new: true },
    );
    console.log('follower update' + followerUpdate);
    const followingUpdate = await userSchema.findByIdAndUpdate(
      req.user.id,
      {
        $push: { following: req.params.id },
      },
      { new: true },
    );
    if (followerUpdate && followingUpdate) {
      otherHelper.sendResponse(res, httpStatus.OK, true, { followerUpdate, followingUpdate }, null, 'Followed Successfully');
    }
  } catch (err) {
    next(err);
  }
};
userController.unfollow = async (req, res, next) => {
  try {
    const followerUpdate = await userSchema.updateOne(
      { _id: req.params.id },
      {
        $pull: {
          follower: req.user.id,
        },
      },
      { new: true },
    ).lean;
    const followingUpdate = await userSchema.updateOne(
      { _id: req.user.id },
      {
        $pull: { following: req.params.id },
      },
      { new: true },
    ).lean;
    console.log(followerUpdate);
    console.log(followingUpdate);
    if (followerUpdate && followingUpdate) {
      otherHelper.sendResponse(res, httpStatus.OK, true, { followerUpdate, followingUpdate }, null, 'Unfollowed Successfully');
    }
  } catch (err) {
    next(err);
  }
};

userController.getUser = async (req, res, next) => {
  try {
    console.log(req.user.id);
    let user = await userSchema.findOne({ _id: req.user.id }).select({ name: 1, follower: 1, following: 1, _id: 0 });
    console.log('user' + user);
    let followerCount = user.follower.length;
    let followingCount = user.following.length;

    return otherHelper.sendResponse(res, httpStatus.OK, true, { name: user.name, followers: followerCount, followings: followingCount }, null, 'user get successful');
  } catch (err) {
    next(err);
  }
};

userController.getAllPosts = async (req, res, next) => {
  try {
    let posts = await postSchema.find({ added_by: req.user.id }).select('_id title description added_at likes').populate({ path: 'comment', select: 'comment' }).sort({ added_at: 1 });
    return otherHelper.sendResponse(res, httpStatus.OK, true, posts, null, 'All posts by user get successful');
  } catch (err) {
    next(err);
  }
};

module.exports = userController;
