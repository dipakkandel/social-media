const http = require('http');
const app = require('./app');
const port = process.env.PORT || 3000;
const server = http.createServer(app);

app.set('PORT_NUMBER', port);

server.listen(port, () => {
  const date = new Date();
  console.log('|---------------------------');
  console.log('| Port      : ' + port);
  console.log('| Date      : ' + date.toJSON().split('T').join(''));
  console.log('|---------------------------');
  console.log('| Waiting for Database Connection');
});

module.exports = server;
