const httpStatus = require('http-status');
const otherHelper = require('../helper/other.helper');
const jwt = require('jsonwebtoken');
let authMiddleware = {};

authMiddleware.authentication = async (req, res, next) => {
  try {
    const secretOrKey = process.env.JWT_SECRET;
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization || req.headers.token;
    if (token && token.length) {
      token = token.replace('Bearer ', '');
      const d = await jwt.verify(token, secretOrKey);
      console.log(d);
      req.user = d;
      return next();
    }
    return otherHelper.sendResponse(res, httpStatus.UNAUTHORIZED, false, null, token, 'token not found', null);
  } catch (err) {
    return otherHelper.sendResponse(res, httpStatus.FORBIDDEN, false, null, null, 'token expired');
  }
};
module.exports = authMiddleware;
