require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const bodyParser = require('body-parser');
const hpp = require('hpp');
const mongoURI = process.env.MONGODB_URI;
const app = express();
const otherHelper = require('./helper/other.helper');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(hpp());
Promise.resolve(app)
  .then(MongoDBConnection())
  .catch((err) => console.error.bind(console, `MongoDB connection error: ${JSON.stringify(err)}`));
async function MongoDBConnection() {
  await mongoose.connect(mongoURI).then(() => {
    console.log(`| MongoDB URL : ${mongoURI}`);
    console.log('| MongoDB Connected');
    console.log('|-----------------------------');
  });
}
//TO USE ROUTES
const routes = require('./routes/index');
app.use(express.static('../client'));
app.use('/api', routes);

//To catch 404 errors and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use((err, req, res, next) => {
  if (err.status === 404) {
    return otherHelper.sendResponse(res, httpStatus.NOT_FOUND, false, null, err, 'Route Not Found');
  } else {
    console.log('\x1b[41m', err);
    return otherHelper.sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, false, null, err, null);
  }
});
module.exports = app;
