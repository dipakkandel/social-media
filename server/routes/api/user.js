const express = require('express');
const router = express.Router();
const userController = require('../../modules/user/userController');
const { authentication } = require('../../middleware/authMiddleware');

//
router.post('/newUser', userController.newUser);
router.get('/allUsers', userController.allUsers);
router.get('/generateToken', userController.returnToken);
router.get('/tokenData', userController.processToken);
router.post('/login', userController.login);
router.post('/follow/:id', authentication, userController.follow);
router.post('/unfollow/:id', authentication, userController.unfollow);
router.get('/user', authentication, userController.getUser);
router.get('/all_posts', authentication, userController.getAllPosts);
module.exports = router;
