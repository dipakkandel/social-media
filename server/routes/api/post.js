const express = require('express');
const router = express.Router();
const postController = require('../../modules/post/postController');
const { authentication } = require('../../middleware/authMiddleware');

router.post('/posts/', authentication, postController.addNewPost);
router.delete('/posts/:id', authentication, postController.deletePost);
router.post('/like/:id', authentication, postController.likePost);
router.post('/unlike/:id', authentication, postController.unLikePost);
router.get('/posts/:id', authentication, postController.getlikescomments);
module.exports = router;
