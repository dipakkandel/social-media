const express = require('express');
const router = express.Router();
const commentController = require('../../modules/comment/commentController');
const { authentication } = require('../../middleware/authMiddleware');

router.post('/comment/:id', authentication, commentController.addComment);
module.exports = router;
