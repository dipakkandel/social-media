const express = require('express');
const router = express.Router();

const userRoutes = require('./api/user');
router.use(userRoutes);
const postRoutes = require('./api/post');
router.use(postRoutes);
const commentRoutes = require('./api/comment');
router.use(commentRoutes);

module.exports = router;
